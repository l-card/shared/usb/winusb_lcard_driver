set scriptpath=%~d0%~p0
set driversrcpath=%scriptpath%\driver
set ddkdir="c:\WinDDK\7600.16385.1"
set inf2cat=%ddkdir%\bin\selfsign\inf2cat
set signtool=%ddkdir%\bin\amd64\SignTool.exe
set crosscert="c:\Program Files (x86)\libs\cert\DigiCertHighAssuranceEVRootCA.crt"
set company="L Card, LLC"
set nsis="C:\Program Files (x86)\NSIS\Bin\makensis.exe"

del lcard_winusb_drv_setup.exe

%nsis% lcard_winusb_drv.nsi
%signtool% sign /v /ac %crosscert% /n %company% /t http://timestamp.digicert.com lcard_winusb_drv_setup.exe

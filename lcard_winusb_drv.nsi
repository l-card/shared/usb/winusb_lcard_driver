SetCompressor /SOLID lzma

Unicode true
RequestExecutionLevel admin

!define DRIVER_VERSION   1.0.0.1
!define INSTALLER_NAME	 lcard_winusb_drv_setup.exe
!define SRC_DRIVER_DIR   driver
!define LDRVINST_DIR     "c:\Program Files (x86)\libs\ldrvinst\"
!define DRV_FILENAME     lcard_winusb

;------------------------------------------------------------------------
;General
!define DISTR_VERSION       "${DRIVER_VERSION}"

Name  "L-Card WinUSB Driver ${DISTR_VERSION}"

!define VENDOR              "L-Card"
!define DISTR_INSTALL_NAME  "lcard_winusb_drv"

!define DISTR_DESCR_NAME    "L-Card WinUSB driver" 
!define FILE_DESCR_NAME     "Installer for ${DISTR_DESCR_NAME}"
!define START_MENU_DIR      "$SMPROGRAMS\${DISTR_DESCR_NAME}"
!define START_MENU_MAN_DIR  "${START_MENU_DIR}\$(manualDir)"


!define REGKEY_VENDOR   "Software\L-Card"
!define REGKEY_DISTR    ${REGKEY_VENDOR}\${DISTR_INSTALL_NAME}
!define REGKEY_UNINST   "Software\Microsoft\Windows\CurrentVersion\Uninstall\${DISTR_INSTALL_NAME}"

!define TMP_DIR         $TEMP\${DISTR_INSTALL_NAME}


;-- Multiuser Settings
!define MULTIUSER_INSTALLMODE_INSTDIR                        ${VENDOR}\${DISTR_INSTALL_NAME}
!define MULTIUSER_INSTALLMODE_INSTDIR_REGISTRY_KEY           ${REGKEY_DISTR}
!define MULTIUSER_INSTALLMODE_INSTDIR_REGISTRY_VALUENAME    "InstallPath"


; ����������� ����������� ��������� ���������� ��� ������������� �������
!define MULTIUSER_EXECUTIONLEVEL Power
; ��������� �������� ��� ������ ��������� ��� ���� ��� ��� �������� ������������
!define MULTIUSER_MUI
; ��������� ����� ��������� ��� ���� ��� �������� � ������� ������ /AllUsers /CurrentUser
!define MULTIUSER_INSTALLMODE_COMMANDLINE



!include "MultiUser.nsh"
!include "MUI2.nsh"
!include "x64.nsh"
!include "WinVer.nsh"
;!include "UserInfo.nsh"



!define DOT_MAJOR 2
!define DOT_MINOR 0







#TargetMinimalOS 5.0

#�������� ����� �����������
OutFile ${INSTALLER_NAME}



;----------------  Interface settings --------------
!define MUI_ABORTWARNING

!define MUI_LANGDLL_REGISTRY_ROOT             SHCTX
!define MUI_LANGDLL_REGISTRY_KEY              ${REGKEY_DISTR}
!define MUI_LANGDLL_REGISTRY_VALUENAME       "Installer Language"


;---------------- Pages ----------------------------
!define MUI_WELCOMEPAGE_TITLE_3LINES

!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES
!define MUI_FINISHPAGE_TITLE_3LINES
!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES
!define MUI_FINISHPAGE_TITLE_3LINES
!insertmacro MUI_UNPAGE_FINISH


;---------------- Languages -------------------------
!insertmacro MUI_LANGUAGE "Russian"
!insertmacro MUI_LANGUAGE "English"





VIAddVersionKey /LANG=${LANG_ENGLISH} "ProductName"      "${DISTR_DESCR_NAME}"
VIAddVersionKey /LANG=${LANG_ENGLISH} "CompanyName"      "${VENDOR}"
VIAddVersionKey /LANG=${LANG_ENGLISH} "FileDescription"  "${FILE_DESCR_NAME}"
VIAddVersionKey /LANG=${LANG_ENGLISH} "FileVersion"      "${DISTR_VERSION}"
VIAddVersionKey /LANG=${LANG_ENGLISH} "ProductVersion"   "${DISTR_VERSION}"
VIAddVersionKey /LANG=${LANG_ENGLISH} "LegalCopyright"   "� 2015 L-Card Ltd."

VIProductVersion ${DISTR_VERSION}.0



;-------------------- Strings --------------------------------------------
;-------------------- Russian
LangString shortcutUninstall        ${LANG_RUSSIAN} "������� '${DISTR_DESCR_NAME}'"    

LangString errorDrvInstExec         ${LANG_RUSSIAN} "������ ������ ��������� ��������� ��������"  
LangString errorDrvInstRes          ${LANG_RUSSIAN} "���������� �������� ������ ��� ������" 
LangString errorDrvKeyFile          ${LANG_RUSSIAN} "�� ���� ��������� ��������� ���� � ����� � ��������"


;-------------------- English ---------------------------------------------
LangString shortcutUninstall        ${LANG_ENGLISH} "Uninstall '${DISTR_DESCR_NAME}'"   

LangString errorDrvInstExec         ${LANG_ENGLISH} "Cannot execute driver installer"
LangString errorDrvInstRes          ${LANG_ENGLISH} "Driver installer return error code" 
LangString errorDrvKeyFile          ${LANG_ENGLISH} "Cannot read temporary driver path file!"

;-------------------- Reserve Files ----------------------------------------
!insertmacro MUI_RESERVEFILE_LANGDLL



; Return on top of stack the total size of the selected (installed) sections, formated as DWORD
; Assumes no more than 256 sections are defined
Var GetInstalledSize.total
Function GetInstalledSize
    ClearErrors
    Push $0
    Push $1
    StrCpy $GetInstalledSize.total 0
    ${ForEach} $1 0 256 + 1
        ${if} ${SectionIsSelected} $1
            SectionGetSize $1 $0
            IntOp $GetInstalledSize.total $GetInstalledSize.total + $0
        ${Endif}
 
        ; Error flag is set when an out-of-bound section is referenced
        ${if} ${errors}
            ${break}
        ${Endif}
    ${Next}
 
    ClearErrors
    Pop $1
    Pop $0
    IntFmt $GetInstalledSize.total "0x%08X" $GetInstalledSize.total
    Push $GetInstalledSize.total
FunctionEnd




Function .onInit
    !insertmacro MULTIUSER_INIT
    !insertmacro MUI_LANGDLL_DISPLAY
FunctionEnd

Function un.onInit
    !insertmacro MULTIUSER_UNINIT
    !insertmacro MUI_UNGETLANGUAGE
FunctionEnd

!macro checkDrvReboot UN
Function ${UN}checkDrvReboot
    Push $0
    Push $1
    #���������, ����� �� ������������
    ClearErrors
    FileOpen $0 reboot r
    IfErrors no_reboot
    FileRead $0 $1
    IfErrors no_reboot
    DetailPrint "need reboot: $1"
    StrCmp $1 "0" no_reboot
    SetRebootFlag true
no_reboot:
    Pop $1
    Pop $0
FunctionEnd
!macroend


!insertmacro checkDrvReboot ""
!insertmacro checkDrvReboot "un."


Var errorString
Var drvUninst


Section -sectDriverUsb

    #�������� �� ��������� ���������� �����, ����������� ��� ��������� ��������
    SetOutPath $INSTDIR\driver
${IfNot} ${AtLeastWinVista}
    DetailPrint "Use WinXP Cat file"
    File /oname=${DRV_FILENAME}.cat "${SRC_DRIVER_DIR}\${DRV_FILENAME}_xp.cat"
${Else}
    DetailPrint "Use Win7 Cat file"
    File "${SRC_DRIVER_DIR}\${DRV_FILENAME}.cat"
${EndIf}
    File "${SRC_DRIVER_DIR}\${DRV_FILENAME}.inf"
    SetOutPath $INSTDIR\driver\x86
    File "${SRC_DRIVER_DIR}\x86\WdfCoInstaller01009.dll"
    File "${SRC_DRIVER_DIR}\x86\winusbcoinstaller2.dll"
    SetOutPath $INSTDIR\driver\amd64
    File "${SRC_DRIVER_DIR}\amd64\WdfCoInstaller01009.dll"
    File "${SRC_DRIVER_DIR}\amd64\winusbcoinstaller2.dll"

    SetOutPath ${TMP_DIR}
    ${If} ${RunningX64}
        File "${LDRVINST_DIR}\amd64\DIFxAPI.dll"
        File "${LDRVINST_DIR}\amd64\ldrvinst.exe"
    ${Else}
        File "${LDRVINST_DIR}\i386\DIFxAPI.dll"
        File "${LDRVINST_DIR}\i386\ldrvinst.exe"
    ${Endif}

driver_stup:
    ClearErrors
    StrCpy $drvUninst "0"

    #���������, ���� �� ���� � ������� � ��������� �������������� inf-�����
    ReadRegStr $2 SHCTX ${REGKEY_DISTR} "DriverUsbInfPath"
    #���� ����, �� ������� ��� ������� �� �������������, ����� - ������� ������� �� inf-����� ����� ���������
    IfErrors skip_uninst
    StrCpy $drvUninst "1"


skip_uninst:
    #������ ����� ������
    ClearErrors
    ExecWait '"${TMP_DIR}\ldrvinst.exe" i "$INSTDIR\driver\${DRV_FILENAME}.inf" reg_key.txt' $0
    #���������, ��� ������ ���� �� ��������� ���������
    IfErrors setup_exec_err
    #��������� ���������, ������������ ����������
    IntCmp $0 0 check_key setup_res_err setup_res_err
check_key:
    #������ ���� ������� ��� �������������� ��������, ����������� � �����
    ClearErrors
    FileOpen $0 reg_key.txt r
    IfErrors setup_file_errs
    FileRead $0 $1
    IfErrors setup_file_errs
    DetailPrint "driver store path: $1"
    WriteRegStr SHCTX ${REGKEY_DISTR} "DriverUsbInfPath" $1
    FileClose $0

    #���������, ����� �� ������������
    Call checkDrvReboot

    #���������, ����� �� ������� ������ �������
    StrCmp $drvUninst "0" done
    #�������, ������ ���� � ������ �������������� ����� inf
    StrCmp $1 $2 done
    ExecWait '"${TMP_DIR}\ldrvinst.exe" u $2' $0
    DetailPrint "uninstall driver with code $0"
    Call checkDrvReboot

    Goto done

setup_exec_err:
    StrCpy $errorString $(errorDrvInstExec)
    Goto setup_err
setup_res_err:
    StrCpy $errorString "$(errorDrvInstRes) $0"
    Goto setup_err
setup_file_errs:
    StrCpy $errorString "$(errorDrvKeyFile)"
    Goto setup_err
setup_err:
    MessageBox MB_ABORTRETRYIGNORE $errorString  /SD IDABORT IDRETRY driver_stup IDIGNORE done

    SetOutPath $TEMP
    RmDir /r ${TMP_DIR}
    Abort
done:
    #�������� ������� ����������, ��� ��� ����� ��� �� ��������
    SetOutPath $TEMP
    #������� ��������� ����������
    RmDir /r ${TMP_DIR}
SectionEnd

Section -sectStartShortcut
    CreateDirectory "${START_MENU_DIR}"
    CreateShortCut  "${START_MENU_DIR}\$(shortcutUninstall).lnk" "$INSTDIR\uninstall.exe" "/$MultiUser.InstallMode"
SectionEnd


; ������� Uninstaller - ������ ������� ��� � ����� ������, ���������� �� ���������  ������
Section -uninstalSection
    #������� Uninstaller
    SetOutPath $INSTDIR
    WriteUninstaller "$INSTDIR\uninstall.exe"
    SetFileAttributes "$INSTDIR\uninstall.exe" HIDDEN

    
    #������� ������ � ������ ��������
    WriteRegStr SHCTX ${REGKEY_UNINST} "DisplayName" "${DISTR_DESCR_NAME}"
    WriteRegStr SHCTX ${REGKEY_UNINST} "UninstallString" "$\"$INSTDIR\uninstall.exe$\" /$MultiUser.InstallMode"
    WriteRegStr SHCTX ${REGKEY_UNINST} "QuietUninstallString" "$\"$INSTDIR\uninstall.exe$\" /$MultiUser.InstallMode /S"
    WriteRegStr SHCTX ${REGKEY_UNINST} "InstallLocation" "$INSTDIR"
    WriteRegStr SHCTX ${REGKEY_UNINST} "Publisher" "${VENDOR}"
    WriteRegStr SHCTX ${REGKEY_UNINST} "DisplayVersion" "${DISTR_VERSION}"
    WriteRegStr SHCTX ${REGKEY_UNINST} "URLInfoAbout" "http://www.lcard.ru"
    WriteRegDWORD SHCTX ${REGKEY_UNINST} "NoModify" 1
    WriteRegDWORD SHCTX ${REGKEY_UNINST} "NoRepair" 1

    Call GetInstalledSize
    Var /GLOBAL distr_size
    Pop $distr_size
    ;IntFmt $distr_size "%d" $distr_size
    ;DetailPrint "Total size =  $distr_size"

    WriteRegDWORD SHCTX ${REGKEY_UNINST} "EstimatedSize" $distr_size

    ;��������� ���� � ����������
    WriteRegStr SHCTX ${MULTIUSER_INSTALLMODE_INSTDIR_REGISTRY_KEY} ${MULTIUSER_INSTALLMODE_INSTDIR_REGISTRY_VALUENAME} "$INSTDIR"
SectionEnd


; ��������, ����������� ��� �������� ���������
Section "Uninstall" SectUninstall

    #������ ���� � ����� � inf-����� ��������
    ClearErrors
    SetOutPath ${TMP_DIR}
    ${If} ${RunningX64}
        File "${LDRVINST_DIR}\amd64\DIFxAPI.dll"
        File "${LDRVINST_DIR}\amd64\ldrvinst.exe"
    ${Else}
        File "${LDRVINST_DIR}\i386\DIFxAPI.dll"
        File "${LDRVINST_DIR}\i386\ldrvinst.exe"
    ${Endif}


    #������ ���� � ����� � inf-����� ��������
    ClearErrors
    ReadRegStr $1 SHCTX ${REGKEY_DISTR} "DriverUsbInfPath"
    IfErrors rem_driver_files
    ExecWait '"${TMP_DIR}\ldrvinst.exe" u $1' $0
    DetailPrint "uninstall driver with code $0"
    DeleteRegValue SHCTX ${REGKEY_DISTR} "DriverUsbInfPath"
    Call un.checkDrvReboot    

rem_driver_files:
    Delete "$INSTDIR\driver\${DRV_FILENAME}.cat"
    Delete "$INSTDIR\driver\${DRV_FILENAME}.inf"
    Delete "$INSTDIR\driver\x86\WdfCoInstaller01009.dll"
    Delete "$INSTDIR\driver\x86\winusbcoinstaller2.dll"
    RMDir  "$INSTDIR\driver\x86"
    Delete "$INSTDIR\driver\amd64\WdfCoInstaller01009.dll"
    Delete "$INSTDIR\driver\amd64\winusbcoinstaller2.dll"
    RMDir  "$INSTDIR\driver\amd64"
    RMDir  "$INSTDIR\driver"
   

    Delete "${START_MENU_DIR}\$(shortcutUninstall).lnk"
    RMDir  "${START_MENU_DIR}"

    ;DeleteRegKey /ifempty SHCTX ${REGKEY_DISTR}
    DeleteRegKey /ifempty SHCTX ${REGKEY_VENDOR}
    DeleteRegKey SHCTX ${REGKEY_UNINST}
    ;DeleteRegValue SHCTX "${MUI_LANGDLL_REGISTRY_KEY}" "${MUI_LANGDLL_REGISTRY_VALUENAME}"
    ;DeleteRegKey /ifempty SHCTX ${REGKEY_DISTR}

    SetOutPath $TEMP
    RmDir /r ${TMP_DIR}

    Delete "$INSTDIR\uninstall.exe"
    RMDir "$INSTDIR"
SectionEnd


